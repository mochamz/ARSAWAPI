<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchCtrl extends Controller
{

    public function search(Request $request)
    {
        $curLat = $request->input('geo_lat')?$request->input('geo_lat'):null;
        $curLng = $request->input('geo_lng')?$request->input('geo_lng'):null;
        $hotel = $request->input('hotel')?$request->input('hotel'):'inactive';
        $t_makan = $request->input('t_makan')?$request->input('t_makan'):'inactive';
        $t_wisata = $request->input('t_wisata')?$request->input('t_wisata'):'inactive';
        $price_start = $request->input('price_start')?$request->input('price_start'):0;
        $price_end = $request->input('price_end')?$request->input('price_end'):4000000;
        $radius = $request->input('radius')?$request->input('radius'):3000;
        $keyword = $request->input('keyword')?$request->input('keyword'):null;
        $radius_fix = ($radius/1470)*1000;
        $query = DB::table('m_place');
        $query->selectRaw('m_place.*, earth_distance(ll_to_earth('.$curLat.', '.$curLng.'), ll_to_earth(m_place.geo_lat, m_place.geo_lng)) as distance');
        $query->whereRaw('earth_box(ll_to_earth('.$curLat.', '.$curLng.'), '.$radius_fix.') @> ll_to_earth(m_place.geo_lat, m_place.geo_lng)');
        $query->where(function ($q) use ($hotel, $t_makan, $t_wisata) {
            if ($hotel === 'active') {
                $q->orWhere('type', 'ilike', '%lodging%');
            }
            if ($t_makan === 'active') {
                $q->orWhere('type', 'ilike', '%food%');
                $q->orWhere('type', 'ilike', '%restaurant%');
            }
            if ($t_wisata === 'active') {
                $q->orWhere('type', 'ilike', '%zoo%');
                $q->orWhere('type', 'ilike', '%amusement_park%');
                $q->orWhere('type', 'ilike', '%aquarium%');
            }
        });
        if ($keyword) {
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'ilike', '%'.$keyword.'%');
                $q->orWhere('address', 'ilike', '%'.$keyword.'%');
            });
        }
        if ($price_end) {
            $query->where(function ($q) use ($price_start) {
                $q->where('start_price', '>=', $price_start);
                $q->orWhere('start_price', null);
            });
            $query->where(function ($q) use ($price_end) {
                $q->where('end_price', null);
                $q->orWhere('end_price', '<=', $price_end);
            });
        }
        $query->orderBy('distance');
        $query->orderBy('start_price');
        $data = $query->get();

        return $data;
    }

    public function detail(Request $request){
        $place_id = $request->input('place_id')?$request->input('place_id'):null;

        $query = DB::table('m_place');
        $query->where('id', $place_id);
        $place = $query->first();

        $query = DB::table('m_review');
        $query->where('place', $place_id);
        $review = $query->get();

        $query = DB::table('m_photo');
        $query->where('place', $place_id);
        $photo = $query->get();

        $data = [
            "place" => $place,
            "review" => $review,
            "photo" => $photo,
        ];

        return $data;
    }
}
