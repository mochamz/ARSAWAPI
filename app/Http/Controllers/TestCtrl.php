<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TestCtrl extends Controller
{

	public function index(Request $request)
	{
		$kelipatan = 5000;

		$query = DB::table('m_place');
		$query->where('type', 'ilike', '%restaurant%');
		$place = $query->get();
		$total = $place->count();
		$i=0;
		// return $place;
		foreach ($place as $row) {
			$rand_min = rand(10,30);
			$rand_max = rand(60,100);
			$hasil_min = $kelipatan*$rand_min;
			$hasil_max = $kelipatan*$rand_max;
			$update = [
				"start_price" => $hasil_min,
				"end_price" => $hasil_max,
			];
			DB::beginTransaction();
			try {
				DB::table('m_place')->where('id', $row->id)->update($update);
				DB::commit();
				$i++;
			} catch (Exception $e) {
				DB::rollback();
			}
			// print_r($update);
		}
		return "done : total = ".$total.", save = ".$i;
	}
}
